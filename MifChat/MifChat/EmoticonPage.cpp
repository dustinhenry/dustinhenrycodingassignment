#include "EmoticonPage.h"
#include "EmoticonsParser.h"
#include "ChatController.h"
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QMessageBox>

EmoticonPage* EmoticonPage::pInstance = 0;

EmoticonPage::EmoticonPage(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	// set up the network manager for retreiving the graphics
	m_pNetworkManager = new QNetworkAccessManager(this);
	connect(m_pNetworkManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(NetworkManagerFinished(QNetworkReply*)));

	QObject::connect(ui.tableWidget, SIGNAL(clicked(QModelIndex)), this, SLOT(EmoticonSelected(QModelIndex)));

	// Set up the emoticon parser
	m_pEmoticonsParser = EmoticonsParser::GetInstance();
	m_pEmoticonsParser->Init("Resources\\xml\\emoticons.xml");
	m_pEmoticonMap = m_pEmoticonsParser->GetEmoticonMap();

	// Get All of the graphics for the emoticons
	for each(auto item in *(m_pEmoticonsParser->GetEmoticonList()))
	{
		// create a network request for the image
		QUrl url(m_pEmoticonMap->find(item)->second.stSRC.c_str());
		QNetworkRequest request(url);
		// create a new object with the name of the emoticon in order to retrieve it from the reply
		QObject* org = new QObject();
		org->setObjectName(item.c_str());
		request.setOriginatingObject(org);
		// Make the request
		m_pNetworkManager->get(request);
	}
}

EmoticonPage::~EmoticonPage()
{
	// clear out the emoticon parser
	m_pEmoticonsParser->DeleteInstance();
}

QPixmap EmoticonPage::GetImagePixmap(QString name)
{
	if (m_imageMap.count(name.toStdString()))
	{
		return m_imageMap[name.toStdString()];
	}
	return QPixmap();
}

void EmoticonPage::EmoticonSelected(QModelIndex)
{
	QWidget::hide();

	auto selected = ui.tableWidget->selectedItems();
	if (selected.count())
	{
		ChatController::GetInstance()->AddEmoticon(selected[1]->text());
	}
}

void EmoticonPage::Show()
{
	QWidget::show();
	// clear the table
	ui.tableWidget->clear();
	ui.tableWidget->setColumnCount(2);
	ui.tableWidget->setRowCount(m_pEmoticonMap->size());
	ui.tableWidget->setColumnWidth(0, 40);
	ui.tableWidget->setColumnWidth(2, 160);

	int row = 0;
	// add all of the emoticons
	for each(auto item in *(m_pEmoticonsParser->GetEmoticonList()))
	{
		QTableWidgetItem* graphic = new QTableWidgetItem();
		graphic->setData(Qt::DecorationRole, m_imageMap[item]);
		QTableWidgetItem* name = new QTableWidgetItem(item.c_str());

		ui.tableWidget->setItem(row, 0, graphic);
		ui.tableWidget->setItem(row, 1, name);
		row++;
	}

}

void EmoticonPage::NetworkManagerFinished(QNetworkReply *reply)
{
	static bool errorShown = false;
	if (reply->error() != QNetworkReply::NoError)
	{
		if (!errorShown)
		{
			QMessageBox::critical(this, "Emoticon Download Error", reply->errorString());
			errorShown = true;
		}
		return;
	}
	// Get the name of the emoticon from the reply
	QString name = reply->request().originatingObject()->objectName();
	// read the image data from the stream
	QByteArray imageData = reply->readAll();
	// Create a new pixmap
	QPixmap pixmap;
	pixmap.loadFromData(imageData);
	// add it to our map
	m_imageMap[name.toStdString()] = pixmap;
}
