#ifndef MIFCHAT_H
#define MIFCHAT_H

#include <QtWidgets/QMainWindow>
#include "ui_MifChat.h"
#include "ChatUser.h"

class UserController;
class JSONController;
class ChatController;
class WelcomePage;
class ChangeUsername;

class MifChat : public QMainWindow
{
	Q_OBJECT

public:
	MifChat(QWidget *parent = 0);
	~MifChat();

	QString GetPrimaryUser() { return m_stPrimaryUser; }

	void SetPrimaryUser(QString name);

public slots:
	void WelcomPageSubmitted(QString username, QColor userColor);
	void ChangePrimaryUser(QString name, QColor color);
	void ClearQSettings();

private:
	Ui::MifChatClass ui;

	UserController*		m_pUserController;
	ChatController*		m_pChatController;
	JSONController*		m_pJSONController;

	WelcomePage*		m_pWelcomePage;
	ChangeUsername*		m_pNameChanger;

	QString				m_stPrimaryUser;
};

#endif // MIFCHAT_H
