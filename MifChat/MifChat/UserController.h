#ifndef USERCONTROLLER_H
#define USERCONTROLLER_H

#include <QWidget>
#include <QTimer>
#include "ui_UserController.h"
#include "ChatUser.h"

class AddUser;

// Class to describe a user
struct ChatUser
{
	ChatUser()
	{
		m_newMentions = 0;
	}

	ChatUser(QString userName, QColor userColor) :
		m_userName(userName), m_userColor(userColor)
	{
		m_newMentions = 0;
	}

	bool operator== (const ChatUser& u)
	{
		return (u.m_userName == m_userName && u.m_userColor == m_userColor);
	}

	QString m_userName;
	QColor  m_userColor;
	int		m_newMentions;
};

class UserController : public QWidget
{
	Q_OBJECT

public:
	~UserController(void);

	static void DeleteInstance(void)
	{
		if (pInstance)
		{
			delete pInstance;
			pInstance = 0;
		}
	}
	static UserController *GetInstance()
	{
		// Check to see if it has been made
		if (pInstance == 0)
			pInstance = new UserController();
		// Return the address of the instance
		return pInstance;
	}
	 
	void UpdateUserList();
	void SetPrimaryUser(ChatUser user);
	void SetPrimaryUser(QString username, QColor userColor) { SetPrimaryUser(ChatUser(username, userColor)); }
	ChatUser GetPrimaryUser() { return m_primaryUser; }
	QColor GetUserColor(QString username);

public slots:
	void ShowUserDialog();
	void AddUserToList(QString name, QColor color);
	void RemoveUser();
	void SwitchCurrentUser(QModelIndex index);
	void MentionUser(QString username);

signals:
	void primaryUserChanged();

private:
	// Instance of the class
	static UserController *pInstance;
	// Make it a singleton by declaring constructors private
	UserController();
	UserController(const UserController&);
	UserController operator = (const UserController&);

	Ui::UserController ui;

	QList<ChatUser>		m_userList;
	AddUser*			m_addUserDialog;
	ChatUser			m_primaryUser;
	QTimer*				m_pClickTimer;
};

#endif // USERCONTROLLER_H
