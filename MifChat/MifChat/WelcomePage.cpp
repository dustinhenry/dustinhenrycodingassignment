﻿#include "WelcomePage.h"
#include <QTimeLine>
#include <QTimer>
#include <QGraphicsOpacityEffect>
#include <qsettings>

#define FADE_TIME 1000

WelcomePage::WelcomePage(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	QObject::connect(ui.submitButton, SIGNAL(clicked()), this, SLOT(SubmitPage()));

	QSettings settings("HardSoft", "MifChat");
	m_bWelcomBack = settings.contains("PrimaryUserName");
	if (m_bWelcomBack)
	{
		m_stWelcomBackUser = settings.value("PrimaryUserName").toString();
		m_userColor.setRed(settings.value("PrimaryUserColorRed").toInt());
		m_userColor.setGreen(settings.value("PrimaryUserColorGreen").toInt());
		m_userColor.setBlue(settings.value("PrimaryUserColorBlue").toInt());
	}
	else
	{
		m_userColor = QColor(255,255,255);
	}
}

WelcomePage::~WelcomePage()
{

}

void WelcomePage::Show()
{
	QWidget::show();

	// hide the elements we will fade in
	QGraphicsOpacityEffect * effect1 = new QGraphicsOpacityEffect(ui.welcomLabel);
	effect1->setOpacity(0);
	ui.welcomLabel->setGraphicsEffect(effect1);
	QGraphicsOpacityEffect * effect2 = new QGraphicsOpacityEffect(ui.usernameBox);
	effect2->setOpacity(0);
	ui.usernameBox->setGraphicsEffect(effect2);

	if (m_bWelcomBack)
	{
		ui.welcomLabel->setText(QString("Welcome back @%1, let us get started shall we?").arg(m_stWelcomBackUser));
	}

	// create a timline to fade in the welcome label over FADE_TIME
	QTimeLine *timeLine = new QTimeLine(FADE_TIME, this);
	timeLine->setFrameRange(0, 100);
	QObject::connect(timeLine, SIGNAL(frameChanged(int)), this, SLOT(FadeInWelcome(int)));
	QObject::connect(timeLine, SIGNAL(finished()), this, SLOT(WelcomeFinished()));
	timeLine->start();
}

void WelcomePage::FadeInWelcome(int frame)
{
	QGraphicsOpacityEffect * effect = new QGraphicsOpacityEffect(ui.welcomLabel);
	effect->setOpacity(frame / 100.0);
	ui.welcomLabel->setGraphicsEffect(effect);
}

void WelcomePage::WelcomeFinished()
{
	if (m_bWelcomBack)
	{
		ui.usernameEdit->setText(m_stWelcomBackUser);
		QTimer::singleShot(1000, this, SLOT(SubmitPage()));
	}
	else
	{
		// create a timline to fade in the user bax over 1.5 seconds
		QTimeLine *timeLine = new QTimeLine(FADE_TIME, this);
		timeLine->setFrameRange(0, 100);
		QObject::connect(timeLine, SIGNAL(frameChanged(int)), this, SLOT(FadeInUser(int)));
		// wait 1 second before starting the fade
		QTimer::singleShot(1000, timeLine, SLOT(start()));
	}
}

void WelcomePage::FadeInUser(int frame)
{
	QGraphicsOpacityEffect * effect = new QGraphicsOpacityEffect(ui.usernameBox);
	effect->setOpacity(frame / 100.0);
	ui.usernameBox->setGraphicsEffect(effect);
}

void WelcomePage::SubmitPage()
{
	if (ui.usernameEdit->text().isEmpty())
	{
		ui.errorLabel->setText("Come on Sparky, you can do it.  Gimme a user name.");
	}
	else
	{
		QString name = ui.usernameEdit->text();
		if (name.startsWith('@'))
		{
			name.remove('@');
		}
		emit Submitted(name, m_userColor);
	}
}

