#include "ChangeUsername.h"
#include "UserController.h"
#include <QColorDialog>

ChangeUsername::ChangeUsername(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	QObject::connect(ui.colorButton, SIGNAL(pressed()), this, SLOT(LaunchColorPicker()));
	QObject::connect(ui.submitButton, SIGNAL(pressed()), this, SLOT(ChangeUser()));
}

ChangeUsername::~ChangeUsername()
{

}

void ChangeUsername::Show()
{
	QWidget::show();

	ChatUser user = UserController::GetInstance()->GetPrimaryUser();

	ui.usernameEdit->setText(user.m_userName);
	m_userColor = user.m_userColor;
	ui.colorButton->setStyleSheet(QString("background:rgb(%1,%2,%3);").arg(m_userColor.red()).arg(m_userColor.green()).arg(m_userColor.blue()));
}

void ChangeUsername::LaunchColorPicker()
{
	m_userColor = QColorDialog::getColor();
	ui.colorButton->setStyleSheet(QString("background:rgb(%1,%2,%3);").arg(m_userColor.red()).arg(m_userColor.green()).arg(m_userColor.blue()));
}

void ChangeUsername::ChangeUser()
{
	if (ui.usernameEdit->text().isEmpty())
	{
	}
	else
	{
		QString name = ui.usernameEdit->text();
		if (name.startsWith('@'))
		{
			name.remove('@');
		}
		emit userChanged(name, m_userColor);
		this->close();
	}
}