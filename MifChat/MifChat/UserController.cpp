#include "UserController.h"
#include "AddUser.h"
#include <QSettings>
#include <QMessageBox>

UserController* UserController::pInstance = 0;

UserController::UserController()
	: QWidget(nullptr)
{
	ui.setupUi(this);

	m_pClickTimer = new QTimer();
	m_pClickTimer->setSingleShot(true);
	m_pClickTimer->setInterval(2000);

	QObject::connect(ui.addUserButton, SIGNAL(pressed()), this, SLOT(ShowUserDialog()));
	QObject::connect(ui.removeUserButton, SIGNAL(pressed()), this, SLOT(RemoveUser()));
	QObject::connect(ui.userTable, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(SwitchCurrentUser(QModelIndex)));
	// After a user clicks the table, deselect after 3 seconds if they don't hit the minus button
	QObject::connect(ui.userTable, SIGNAL(clicked(QModelIndex)), m_pClickTimer, SLOT(start()));
	QObject::connect(m_pClickTimer, SIGNAL(timeout()), ui.userTable, SLOT(clearSelection()));
}

UserController::~UserController()
{

}

void UserController::SetPrimaryUser(ChatUser user)
{
	QSettings settings("HardSoft", "MifChat");
	settings.setValue("PrimaryUserName", user.m_userName);
	settings.setValue("PrimaryUserColorRed", user.m_userColor.red());
	settings.setValue("PrimaryUserColorGreen", user.m_userColor.green());
	settings.setValue("PrimaryUserColorBlue", user.m_userColor.blue());

	m_primaryUser = user;
	ui.userLabel->setText(QString("Hi @%1, double click a friend to chat as them.").arg(user.m_userName));

	emit primaryUserChanged();
}

void UserController::ShowUserDialog()
{
	m_addUserDialog = new AddUser();
	m_addUserDialog->show();

	QObject::connect(m_addUserDialog, SIGNAL(userAdded(QString, QColor)), this, SLOT(AddUserToList(QString, QColor)));
}

void UserController::AddUserToList(QString name, QColor color)
{
	ChatUser newUser(name, color);
	m_userList.append(newUser);
	UpdateUserList();
}

void UserController::RemoveUser()
{
	m_pClickTimer->stop();
	if (QMessageBox::question(nullptr, "Remove User", "Are you sure you want to remove this user?") == QMessageBox::Yes)
	{
		auto selected = ui.userTable->selectedItems();
		if (selected.count())
		{
			ChatUser user(selected[1]->text().remove('@'), selected[0]->backgroundColor());
			m_userList.removeOne(user);

			UpdateUserList();
		}
	}
	ui.userTable->clearSelection();
}

void UserController::SwitchCurrentUser(QModelIndex index)
{
	auto selected = ui.userTable->selectedItems();
	if (selected.count())
	{
		ChatUser user(selected[1]->text().remove('@'), selected[0]->backgroundColor());
		m_userList.replace(index.row(), m_primaryUser);

		UpdateUserList();

		SetPrimaryUser(user);
	}
}

void UserController::UpdateUserList()
{
	// clean up the table
	ui.userTable->clear();

	if (m_userList.count())
	{
		// set up the table dimensions
		ui.userTable->setColumnCount(3);
		ui.userTable->setRowCount(m_userList.count());
		ui.userTable->setColumnWidth(0, 30);
		ui.userTable->setColumnWidth(1, ui.userTable->width() - 65);
		ui.userTable->setColumnWidth(2, 30);

		// add all of the individual users
		for (int i = 0; i < m_userList.count(); i++)
		{
			QTableWidgetItem* color = new QTableWidgetItem();
			color->setBackgroundColor(m_userList[i].m_userColor);
			QTableWidgetItem* name = new QTableWidgetItem(QString("@%1").arg(m_userList[i].m_userName));
			QString mention;
			if (m_userList[i].m_newMentions)
			{
				mention = QString("%1").arg(m_userList[i].m_newMentions);
			}
			QTableWidgetItem* mentions = new QTableWidgetItem(mention);

			ui.userTable->setItem(i, 0, color);
			ui.userTable->setItem(i, 1, name);
			ui.userTable->setItem(i, 2, mentions);
		}
	}
}

void UserController::MentionUser(QString username)
{
	bool found = false;
	// slow but since we aren't passing in a color we need to search each user individually
	for (int i = 0; i < m_userList.count(); i++)
	{
		if (m_userList[i].m_userName == username)
		{
			m_userList[i].m_newMentions++;
			found = true;
			UpdateUserList();
		}
	}
	// If a user was mentioned but not in the list, add them
	if (!found && username != m_primaryUser.m_userName)
	{
		AddUserToList(username, QColor(255, 255, 255));
	}
}

QColor UserController::GetUserColor(QString username)
{
	for (int i = 0; i < m_userList.count(); i++)
	{
		if (m_userList[i].m_userName == username)
		{
			return m_userList[i].m_userColor;
		}
	}
	// this should never happen
	return QColor();
}