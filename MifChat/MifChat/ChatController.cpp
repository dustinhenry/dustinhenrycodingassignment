#include "ChatController.h"
#include "EmoticonsParser.h"
#include "EmoticonPage.h"
#include "UserController.h"
#include "JSONController.h"
#include "EmoticonPage.h"

#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QBuffer>

ChatController* ChatController::pInstance = 0;

ChatController::ChatController()
	: QWidget(nullptr)
{
	ui.setupUi(this);
	// set up the network manager for retreiving the graphics
	m_pNetworkManager = new QNetworkAccessManager(this);
	connect(m_pNetworkManager,			SIGNAL(finished(QNetworkReply*)),	this, SLOT(NetworkManagerFinished(QNetworkReply*)));

	m_pEmoticonPage = EmoticonPage::GetInstance(this);
	m_pEmoticonPage->hide();

	m_pEmoticonsParser = EmoticonsParser::GetInstance();
	m_pEmoticonMap = m_pEmoticonsParser->GetEmoticonMap();
	
	m_pUserController = UserController::GetInstance();
	QObject::connect(m_pUserController, SIGNAL(primaryUserChanged()),		this, SLOT(ChangePrimaryUser()));

	QObject::connect(ui.emoticonButton, SIGNAL(pressed()),					this, SLOT(ShowEmoticons()));
	QObject::connect(ui.chatButton,		SIGNAL(pressed()),					this, SLOT(ChatPressed()));
	QObject::connect(ui.chatEdit,		SIGNAL(editingFinished()),			this, SLOT(EditLostFocus()));
	QObject::connect(ui.chatEdit,		SIGNAL(cursorPositionChanged(int, int)), this, SLOT(EditCursorPositionChanged(int, int)));
	QObject::connect(ui.chatEdit,		SIGNAL(returnPressed()),			this, SLOT(ChatPressed()));

	ChangePrimaryUser();

	m_editPosition = 0;
}

ChatController::~ChatController()
{

}

void ChatController::EditLostFocus()
{

}

void ChatController::EditCursorPositionChanged(int old, int current)
{
	m_editPosition = current;
}

void ChatController::AddEmoticon(QString name)
{
	QString text = ui.chatEdit->text();
	text.insert(m_editPosition, QString("%1 ").arg(name));
	ui.chatEdit->setText(text);
	ui.chatEdit->setFocus();	
}

void ChatController::ShowEmoticons()
{
	if (m_pEmoticonPage->isHidden())
	{
		QPoint pos = ui.emoticonButton->pos();
		pos.setX(pos.x() - (m_pEmoticonPage->width() / 2));
		pos.setY(pos.y() - m_pEmoticonPage->height() - 10);

		m_pEmoticonPage->move(pos);
		m_pEmoticonPage->Show();
	}
	else
	{
		m_pEmoticonPage->hide();
	}
}

void ChatController::ChangePrimaryUser()
{
	ChatUser user = m_pUserController->GetPrimaryUser();
	ui.currentUserLabel->setText(QString("@%1").arg(user.m_userName));
	ui.currentUserLabel->setStyleSheet(QString("color:rgb(%1,%2,%3);").arg(user.m_userColor.red()).arg(user.m_userColor.green()).arg(user.m_userColor.blue()));
}

void ChatController::ChatPressed()
{
	std::vector<ChatStringElement> vURLs;
	std::vector<ChatStringElement> vEmoticons;
	std::vector<ChatStringElement> vMentions;

	m_currentChatString = ui.chatEdit->text();

	if (!ui.chatEdit->text().isEmpty())
	{
		// find all of the mentions
		int start = 0;
		int index = m_currentChatString.indexOf('@', start);
		while (index != -1)
		{
			int end = m_currentChatString.indexOf(' ', index);
			ChatStringElement mention(m_currentChatString.mid(index, (end != -1) ? end - index : end), index, end, ChatStringElementType::Mention);
			vMentions.push_back(mention);
			m_pUserController->MentionUser(mention.m_resource.mid(1));
			// search again with a new starting point
			start = index + 1;
			index = m_currentChatString.indexOf('@', start);
		}

		// find all of the emoticons
		start = 0;
		index = m_currentChatString.indexOf('(', start);
		while (index != -1)
		{
			int end = m_currentChatString.indexOf(')', index);
			if (end != -1)
			{
				ChatStringElement emoticon(m_currentChatString.mid(index, end - index + 1), index, end, ChatStringElementType::Emoticon);
				vEmoticons.push_back(emoticon);
			}
			// search again with a new starting point
			start = index + 1;
			index = m_currentChatString.indexOf('(', start);
		}

		// find all of the urls
		// go through first with http://
		start = 0;
		index = m_currentChatString.indexOf("http://", start);
		while (index != -1)
		{
			int end = m_currentChatString.indexOf(' ', index);

			ChatStringElement url(m_currentChatString.mid(index, (end != -1) ? end - index : end), index, end, ChatStringElementType::URL);
			vURLs.push_back(url);

			// search again with a new starting point
			start = index + 1;
			index = m_currentChatString.indexOf("http://", start);
		}
		// now go through with https://
		start = 0;
		index = m_currentChatString.indexOf("https://", start);
		while (index != -1)
		{
			int end = m_currentChatString.indexOf(' ', index);

			ChatStringElement url(m_currentChatString.mid(index, (end != -1) ? end - index : end), index, end, ChatStringElementType::URL);
			vURLs.push_back(url);

			// search again with a new starting point
			start = index + 1;
			index = m_currentChatString.indexOf("https://", start);
		}
	}
	// add the current string to the log
	m_vChatLog.push_back(m_currentChatString);
	// store the values
	m_jsonDocumentContents.vEmoticons = vEmoticons;
	m_jsonDocumentContents.vMentions = vMentions;
	m_jsonDocumentContents.vURLs = vURLs;

	AddMesageToChatList();

	if (m_jsonDocumentContents.vURLs.size())
	{
		// get the titles for the urls
		QueryURLTitles();
	}
	else
	{
		CreateJSONDocument();
	}
	// Do some clean up
	m_currentChatString.clear();
	ui.chatEdit->clear();
	m_editPosition = 0;
}

void ChatController::CreateJSONDocument()
{
	// create the JSON document
	QJsonObject jsonObject;
	if (m_jsonDocumentContents.vEmoticons.size())
	{
		QJsonArray jsonEmoticons;
		for each (auto emoticon in m_jsonDocumentContents.vEmoticons)
		{
			jsonEmoticons.append(emoticon.m_resource.mid(1, emoticon.m_resource.size() - 2));
		}
		jsonObject.insert("emoticons", jsonEmoticons);
	}
	if (m_jsonDocumentContents.vMentions.size())
	{
		QJsonArray jsonMentions;
		for each (auto mention in m_jsonDocumentContents.vMentions)
		{
			jsonMentions.append(mention.m_resource.mid(1));
		}
		jsonObject.insert("mentions", jsonMentions);
	}
	if (m_jsonDocumentContents.vURLs.size())
	{
		QJsonArray jsonURLs;
		for each (auto url in m_jsonDocumentContents.vURLs)
		{
			QJsonObject urlObject;
			urlObject.insert("url", url.m_resource);
			urlObject.insert("title", m_jsonDocumentContents.vURLTitles[url.m_resource]);
			jsonURLs.append(urlObject);
		}
		jsonObject.insert("links", jsonURLs);
	}

	QJsonDocument jsonDocument(jsonObject);
	QString jsonString(jsonDocument.toJson());

	JSONController::GetInstance()->ShowJSON(jsonString);

	m_jsonDocumentContents.Clear();
}

void ChatController::QueryURLTitles()
{
	// Get All of the graphics for the emoticons
	for each(auto urlString in m_jsonDocumentContents.vURLs)
	{
		// create a network request for the image
		QUrl url(urlString.m_resource);
		QNetworkRequest request(url);
		// Make the request
		m_pNetworkManager->get(request);
	}
}

void ChatController::NetworkManagerFinished(QNetworkReply *reply)
{
	// get the request url for adding to the title map
	QString url = reply->request().url().toString();
	if (reply->error() != QNetworkReply::NoError)
	{
		m_jsonDocumentContents.vURLTitles[url] = "Error retreiving URL title.";
	}
	else
	{
		// read the raw data from the stream
		QByteArray rawData = reply->readAll();
		QString rawDataString(rawData);

		// find the title header and store it
		int start = rawDataString.indexOf("<title>");
		if (start != -1)
		{
			start += 7;
			int end = rawDataString.indexOf("</title>", start + 1);
			if (end != -1)
			{
				QString title = rawDataString.mid(start, end - start);
				m_jsonDocumentContents.vURLTitles[url] = title;
			}
			else
			{
				m_jsonDocumentContents.vURLTitles[url] = "Error retreiving URL title.";
			}
		}
		else
		{
			m_jsonDocumentContents.vURLTitles[url] = "Error retreiving URL title.";
		}
	}
	// Have we got them all?
	if (m_jsonDocumentContents.vURLs.size() == m_jsonDocumentContents.vURLTitles.size())
	{
		CreateJSONDocument();
	}
}

void ChatController::AddMesageToChatList()
{
	QString chatString = m_currentChatString;
	// normally I would load a template file but I'm putting this here for brevity
	QString htmlDocument;
	htmlDocument += "<html>\n";
	htmlDocument += "<style>\n";
	htmlDocument += "	table{\n";
	htmlDocument += "		border-collapse: collapse;\n";
	htmlDocument += "		font-family:Century Gothic;\n";
	htmlDocument += "		font-size:11pt;\n";
	htmlDocument += "		border: 1px solid black;\n";
	htmlDocument += "	}\n";
	htmlDocument += "	mention{\n";
	htmlDocument += "		background-color:rgb(96, 110, 129);\n";
	htmlDocument += "		padding:4px;\n";
	htmlDocument += "	}\n";
	htmlDocument += "	table, td, th{";
	htmlDocument += "		padding:10px;\n";
	htmlDocument += "	}\n";
	htmlDocument += "	#username{\n";
	htmlDocument += "		background-color:rgb(96, 110, 129);\n";
	htmlDocument += "		text-decoration:bold;\n";
	htmlDocument += "		border-color:black;\n";
	htmlDocument += "		border-radius : 5px;\n";
	htmlDocument += "		border-style:solid\n";
	htmlDocument += "	}\n";
	htmlDocument += "</style>\n";
	htmlDocument += "</head>\n";
	htmlDocument += "<body>\n";
	htmlDocument += "	<table border=1>\n";
	htmlDocument += "		<tr>\n";
	htmlDocument += "			<td width=\"100\" id=\"username\" align=\"right\">\n";
	htmlDocument += "				#USERNAME#\n";
	htmlDocument += "			</td>\n";
	htmlDocument += "			<td>\n";
	htmlDocument += "				#CHATSTRING#\n";
	htmlDocument += "			</td>\n";
	htmlDocument += "		</tr>\n";
	htmlDocument += "	</table>\n";
	htmlDocument += "</body>\n";
	htmlDocument += "</html>\n";

	std::list<ChatStringElement> elementList;
	int stretchLength = 0;
	// add all of the elements to a list
	for each (auto element in m_jsonDocumentContents.vEmoticons)
	{
		elementList.push_back(element);
	}
	for each (auto element in m_jsonDocumentContents.vMentions)
	{
		elementList.push_back(element);
	}
	for each (auto element in m_jsonDocumentContents.vURLs)
	{
		elementList.push_back(element);
	}
	// sort the list be start position
	elementList.sort();
	// go over all the elements and add html
	int maxEmoticonHeight = 0;
	for each (auto element in elementList)
	{
		switch (element.m_type)
		{
			case ChatStringElementType::Emoticon:
			{
				QString newString;
				std::string name = element.m_resource.toStdString();
				if (m_pEmoticonMap->count(name))
				{
					if (m_pEmoticonMap->at(name).iHeight > maxEmoticonHeight)
					{
						maxEmoticonHeight = m_pEmoticonMap->at(name).iHeight;
					}
					QString url(m_pEmoticonMap->at(name).stSRC.c_str());    
					// get the pixmap and convert it to a byte array
					QPixmap pixmap = EmoticonPage::GetInstance(0)->GetImagePixmap(QString(name.c_str()));
					QByteArray bytes;
					QBuffer buffer(&bytes);
					buffer.open(QIODevice::WriteOnly);
					pixmap.save(&buffer, "PNG");

					newString = "<img src = \"data:image/png;base64," + bytes.toBase64() + "\"/>";
					//newString = "<img src=\"" + url + "\"/>";
					element.Stretch(stretchLength);
					int length = element.GetLength(m_currentChatString.size()) + 1;
					chatString.replace(element.m_start, length, newString);
					stretchLength += newString.length() - length;


				}
			}
			break;
			case ChatStringElementType::Mention:
			{
				QString newString = QString("<mention><font color=%1>").arg(m_pUserController->GetUserColor(element.m_resource.mid(1)).name());
				newString += element.m_resource + "</font></mention>";
				std::string name = element.m_resource.toStdString();
				element.Stretch(stretchLength);
				int length = element.GetLength(chatString.size());
				chatString.replace(element.m_start, length, newString);
				stretchLength += newString.length() - length;
			}		
			break;
			case ChatStringElementType::URL :
			{
				QString newString = "<a href=\"" + element.m_resource + "\">" + element.m_resource + "</a>";
				element.Stretch(stretchLength);
				int length = element.GetLength(chatString.size());
				chatString.replace(element.m_start, length, newString);
				stretchLength += newString.length() - length;
			}
			break;
		}
	}

	int userStart = htmlDocument.indexOf("#USERNAME#");

	QString newString = QString("<font color=\"%1\">%2</font>").arg(m_pUserController->GetPrimaryUser().m_userColor.name()).arg(m_pUserController->GetPrimaryUser().m_userName);
	htmlDocument.replace(userStart, 10, newString);

	int chatStart = htmlDocument.indexOf("#CHATSTRING#");
	htmlDocument.replace(chatStart, 12, chatString);

	QLabel* chatLabel = new QLabel(htmlDocument);
	chatLabel->setTextFormat(Qt::RichText);
	QListWidgetItem* item = new QListWidgetItem();
	if (maxEmoticonHeight > 0)
	{
		item->setSizeHint(QSize(ui.chatList->width(), maxEmoticonHeight + 33));
	}
	else
	{
		item->setSizeHint(QSize(ui.chatList->width(), 50));
	}
	ui.chatList->addItem(item);
	ui.chatList->setItemWidget(item, chatLabel);

	m_vChatLogHTML.push_back(chatString);
}
