/********************************************************************************
** Form generated from reading UI file 'JSONController.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_JSONCONTROLLER_H
#define UI_JSONCONTROLLER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_JSONController
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *JSONFrame;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_3;
    QLabel *label_8;
    QTextEdit *JSONText;
    QLabel *label_11;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *JSONController)
    {
        if (JSONController->objectName().isEmpty())
            JSONController->setObjectName(QStringLiteral("JSONController"));
        JSONController->resize(300, 801);
        JSONController->setMaximumSize(QSize(300, 16777215));
        JSONController->setStyleSheet(QStringLiteral("QFrame{background-color:rgb(61,83,105);}"));
        verticalLayout = new QVBoxLayout(JSONController);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        JSONFrame = new QFrame(JSONController);
        JSONFrame->setObjectName(QStringLiteral("JSONFrame"));
        JSONFrame->setMinimumSize(QSize(230, 0));
        JSONFrame->setMaximumSize(QSize(300, 16777215));
        QFont font;
        font.setFamily(QStringLiteral("Century Gothic"));
        font.setPointSize(10);
        JSONFrame->setFont(font);
        JSONFrame->setFrameShape(QFrame::Box);
        JSONFrame->setFrameShadow(QFrame::Plain);
        JSONFrame->setLineWidth(0);
        verticalLayout_2 = new QVBoxLayout(JSONFrame);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_3 = new QLabel(JSONFrame);
        label_3->setObjectName(QStringLiteral("label_3"));
        QSizePolicy sizePolicy(QSizePolicy::Ignored, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy);
        label_3->setMinimumSize(QSize(0, 30));
        label_3->setMaximumSize(QSize(16777215, 30));
        QFont font1;
        font1.setFamily(QStringLiteral("Century Gothic"));
        font1.setPointSize(11);
        font1.setBold(false);
        font1.setWeight(50);
        label_3->setFont(font1);
        label_3->setStyleSheet(QStringLiteral("Color:white;"));

        verticalLayout_2->addWidget(label_3);

        label_8 = new QLabel(JSONFrame);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setMaximumSize(QSize(16777215, 9));
        label_8->setStyleSheet(QStringLiteral("background-color:rgb(96,110,129);"));
        label_8->setPixmap(QPixmap(QString::fromUtf8("Resources/shadow.png")));
        label_8->setScaledContents(true);

        verticalLayout_2->addWidget(label_8);

        JSONText = new QTextEdit(JSONFrame);
        JSONText->setObjectName(QStringLiteral("JSONText"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(JSONText->sizePolicy().hasHeightForWidth());
        JSONText->setSizePolicy(sizePolicy1);
        JSONText->setMinimumSize(QSize(200, 290));
        JSONText->setMaximumSize(QSize(16777215, 16777215));
        JSONText->setFont(font);
        JSONText->setStyleSheet(QStringLiteral("background-color:rgb(96,110,129);color:rgb(240,240,240);"));
        JSONText->setFrameShape(QFrame::Panel);
        JSONText->setFrameShadow(QFrame::Plain);
        JSONText->setLineWidth(0);
        JSONText->setUndoRedoEnabled(false);
        JSONText->setLineWrapMode(QTextEdit::NoWrap);
        JSONText->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        verticalLayout_2->addWidget(JSONText);

        label_11 = new QLabel(JSONFrame);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setMinimumSize(QSize(0, 4));
        label_11->setMaximumSize(QSize(16777215, 4));
        label_11->setStyleSheet(QStringLiteral("background-color:rgb(96,110,129);"));
        label_11->setLineWidth(0);
        label_11->setPixmap(QPixmap(QString::fromUtf8("Resources/lower_bevel.png")));
        label_11->setScaledContents(true);

        verticalLayout_2->addWidget(label_11);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Minimum);

        verticalLayout_2->addItem(verticalSpacer);


        verticalLayout->addWidget(JSONFrame);


        retranslateUi(JSONController);

        QMetaObject::connectSlotsByName(JSONController);
    } // setupUi

    void retranslateUi(QWidget *JSONController)
    {
        JSONController->setWindowTitle(QApplication::translate("JSONController", "JSONController", 0));
        label_3->setText(QApplication::translate("JSONController", "   JSON Output", 0));
        label_8->setText(QString());
        JSONText->setHtml(QApplication::translate("JSONController", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Century Gothic'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", 0));
        label_11->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class JSONController: public Ui_JSONController {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_JSONCONTROLLER_H
