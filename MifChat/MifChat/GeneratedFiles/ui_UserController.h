/********************************************************************************
** Form generated from reading UI file 'UserController.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USERCONTROLLER_H
#define UI_USERCONTROLLER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UserController
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *userFrame;
    QGridLayout *gridLayout;
    QSpacerItem *verticalSpacer_2;
    QLabel *userLabel;
    QPushButton *removeUserButton;
    QLabel *label_10;
    QLabel *label_6;
    QPushButton *addUserButton;
    QTableWidget *userTable;
    QLabel *label;
    QLabel *label_2;

    void setupUi(QWidget *UserController)
    {
        if (UserController->objectName().isEmpty())
            UserController->setObjectName(QStringLiteral("UserController"));
        UserController->resize(300, 626);
        UserController->setMaximumSize(QSize(300, 16777215));
        UserController->setStyleSheet(QStringLiteral("QFrame{background-color:rgb(61,83,105);}"));
        verticalLayout = new QVBoxLayout(UserController);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        userFrame = new QFrame(UserController);
        userFrame->setObjectName(QStringLiteral("userFrame"));
        userFrame->setMinimumSize(QSize(170, 0));
        userFrame->setMaximumSize(QSize(300, 16777215));
        QFont font;
        font.setFamily(QStringLiteral("Century Gothic"));
        font.setPointSize(10);
        userFrame->setFont(font);
        userFrame->setStyleSheet(QStringLiteral(""));
        userFrame->setFrameShape(QFrame::Box);
        userFrame->setFrameShadow(QFrame::Plain);
        userFrame->setLineWidth(0);
        gridLayout = new QGridLayout(userFrame);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, -1);
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 7, 1, 1, 1);

        userLabel = new QLabel(userFrame);
        userLabel->setObjectName(QStringLiteral("userLabel"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(userLabel->sizePolicy().hasHeightForWidth());
        userLabel->setSizePolicy(sizePolicy);
        QFont font1;
        font1.setFamily(QStringLiteral("Century Gothic"));
        font1.setPointSize(8);
        userLabel->setFont(font1);
        userLabel->setStyleSheet(QStringLiteral("background-color:rgb(96,110,129);color:rgb(240,240,240);"));
        userLabel->setAlignment(Qt::AlignCenter);
        userLabel->setWordWrap(true);

        gridLayout->addWidget(userLabel, 2, 0, 1, 3);

        removeUserButton = new QPushButton(userFrame);
        removeUserButton->setObjectName(QStringLiteral("removeUserButton"));
        removeUserButton->setMinimumSize(QSize(0, 30));
        removeUserButton->setMaximumSize(QSize(30, 30));
        removeUserButton->setFont(font);

        gridLayout->addWidget(removeUserButton, 6, 1, 1, 1);

        label_10 = new QLabel(userFrame);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setMinimumSize(QSize(0, 4));
        label_10->setMaximumSize(QSize(16777215, 4));
        label_10->setStyleSheet(QStringLiteral("background-color:rgb(96,110,129);"));
        label_10->setLineWidth(0);
        label_10->setPixmap(QPixmap(QString::fromUtf8("Resources/lower_bevel.png")));
        label_10->setScaledContents(true);

        gridLayout->addWidget(label_10, 5, 0, 1, 3);

        label_6 = new QLabel(userFrame);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setStyleSheet(QStringLiteral("background-color:rgb(96,110,129);"));
        label_6->setPixmap(QPixmap(QString::fromUtf8("Resources/shadow.png")));
        label_6->setScaledContents(true);

        gridLayout->addWidget(label_6, 1, 0, 1, 3);

        addUserButton = new QPushButton(userFrame);
        addUserButton->setObjectName(QStringLiteral("addUserButton"));
        addUserButton->setMinimumSize(QSize(0, 30));
        addUserButton->setMaximumSize(QSize(30, 30));
        addUserButton->setFont(font);

        gridLayout->addWidget(addUserButton, 6, 2, 1, 1);

        userTable = new QTableWidget(userFrame);
        userTable->setObjectName(QStringLiteral("userTable"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(userTable->sizePolicy().hasHeightForWidth());
        userTable->setSizePolicy(sizePolicy1);
        userTable->setMinimumSize(QSize(200, 250));
        userTable->setMaximumSize(QSize(16777215, 300));
        userTable->setFont(font);
        userTable->setStyleSheet(QStringLiteral("background-color:rgb(96,110,129);color:rgb(240,240,240);"));
        userTable->setFrameShape(QFrame::Panel);
        userTable->setFrameShadow(QFrame::Plain);
        userTable->setLineWidth(0);
        userTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        userTable->setSelectionMode(QAbstractItemView::SingleSelection);
        userTable->setSelectionBehavior(QAbstractItemView::SelectRows);
        userTable->setShowGrid(false);
        userTable->setWordWrap(false);
        userTable->setCornerButtonEnabled(false);
        userTable->setRowCount(0);
        userTable->horizontalHeader()->setVisible(false);
        userTable->horizontalHeader()->setHighlightSections(false);
        userTable->horizontalHeader()->setProperty("showSortIndicator", QVariant(false));
        userTable->verticalHeader()->setVisible(false);
        userTable->verticalHeader()->setHighlightSections(false);

        gridLayout->addWidget(userTable, 4, 0, 1, 3);

        label = new QLabel(userFrame);
        label->setObjectName(QStringLiteral("label"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy2);
        label->setMinimumSize(QSize(0, 30));
        label->setMaximumSize(QSize(16777215, 30));
        QFont font2;
        font2.setFamily(QStringLiteral("Century Gothic"));
        font2.setPointSize(11);
        font2.setBold(false);
        font2.setWeight(50);
        label->setFont(font2);
        label->setStyleSheet(QStringLiteral("Color:white;"));
        label->setLineWidth(0);

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(userFrame);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMaximumSize(QSize(16777215, 2));
        label_2->setStyleSheet(QStringLiteral("background:lightgray"));
        label_2->setFrameShape(QFrame::NoFrame);
        label_2->setLineWidth(0);
        label_2->setScaledContents(true);
        label_2->setMargin(4);

        gridLayout->addWidget(label_2, 3, 0, 1, 3);


        verticalLayout->addWidget(userFrame);


        retranslateUi(UserController);

        QMetaObject::connectSlotsByName(UserController);
    } // setupUi

    void retranslateUi(QWidget *UserController)
    {
        UserController->setWindowTitle(QApplication::translate("UserController", "UserController", 0));
        userLabel->setText(QString());
        removeUserButton->setText(QApplication::translate("UserController", "-", 0));
        label_10->setText(QString());
        label_6->setText(QString());
        addUserButton->setText(QApplication::translate("UserController", "+", 0));
        label->setText(QApplication::translate("UserController", "   Imaginary Friend List", 0));
        label_2->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class UserController: public Ui_UserController {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USERCONTROLLER_H
