/********************************************************************************
** Form generated from reading UI file 'AddUser.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDUSER_H
#define UI_ADDUSER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AddUser
{
public:
    QHBoxLayout *horizontalLayout;
    QFrame *frame;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QLineEdit *usernameEdit;
    QPushButton *submitButton;
    QLabel *label_3;
    QLabel *errorLabel;
    QPushButton *colorButton;

    void setupUi(QWidget *AddUser)
    {
        if (AddUser->objectName().isEmpty())
            AddUser->setObjectName(QStringLiteral("AddUser"));
        AddUser->resize(300, 160);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(AddUser->sizePolicy().hasHeightForWidth());
        AddUser->setSizePolicy(sizePolicy);
        AddUser->setMinimumSize(QSize(300, 160));
        AddUser->setMaximumSize(QSize(300, 160));
        QFont font;
        font.setFamily(QStringLiteral("Century Gothic"));
        font.setPointSize(11);
        AddUser->setFont(font);
        AddUser->setStyleSheet(QStringLiteral("QFrame{background-color:rgb(61,83,105);}"));
        horizontalLayout = new QHBoxLayout(AddUser);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        frame = new QFrame(AddUser);
        frame->setObjectName(QStringLiteral("frame"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy1);
        frame->setMinimumSize(QSize(300, 160));
        frame->setMaximumSize(QSize(300, 160));
        frame->setFont(font);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout = new QGridLayout(frame);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_2 = new QLabel(frame);
        label_2->setObjectName(QStringLiteral("label_2"));
        QFont font1;
        font1.setFamily(QStringLiteral("Century Gothic"));
        font1.setPointSize(10);
        label_2->setFont(font1);
        label_2->setStyleSheet(QStringLiteral("color:white"));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label = new QLabel(frame);
        label->setObjectName(QStringLiteral("label"));
        label->setFont(font);
        label->setStyleSheet(QStringLiteral("color:white"));
        label->setAlignment(Qt::AlignCenter);
        label->setWordWrap(true);

        gridLayout->addWidget(label, 0, 0, 1, 4);

        horizontalSpacer = new QSpacerItem(59, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 2, 2, 1, 1);

        usernameEdit = new QLineEdit(frame);
        usernameEdit->setObjectName(QStringLiteral("usernameEdit"));
        usernameEdit->setFont(font);
        usernameEdit->setMaxLength(12);

        gridLayout->addWidget(usernameEdit, 1, 1, 1, 3);

        submitButton = new QPushButton(frame);
        submitButton->setObjectName(QStringLiteral("submitButton"));
        submitButton->setFont(font);

        gridLayout->addWidget(submitButton, 2, 3, 1, 1);

        label_3 = new QLabel(frame);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font1);
        label_3->setStyleSheet(QStringLiteral("color:white"));
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        errorLabel = new QLabel(frame);
        errorLabel->setObjectName(QStringLiteral("errorLabel"));
        errorLabel->setEnabled(true);
        QFont font2;
        font2.setFamily(QStringLiteral("Century Gothic"));
        font2.setPointSize(9);
        errorLabel->setFont(font2);
        errorLabel->setStyleSheet(QStringLiteral("color:rgb(250,136,147);"));
        errorLabel->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(errorLabel, 3, 0, 1, 4);

        colorButton = new QPushButton(frame);
        colorButton->setObjectName(QStringLiteral("colorButton"));
        colorButton->setMinimumSize(QSize(30, 25));
        colorButton->setMaximumSize(QSize(30, 25));
        colorButton->setStyleSheet(QStringLiteral("background-color:white"));
        colorButton->setFlat(false);

        gridLayout->addWidget(colorButton, 2, 1, 1, 1);


        horizontalLayout->addWidget(frame);


        retranslateUi(AddUser);

        QMetaObject::connectSlotsByName(AddUser);
    } // setupUi

    void retranslateUi(QWidget *AddUser)
    {
        AddUser->setWindowTitle(QApplication::translate("AddUser", "AddUser", 0));
        label_2->setText(QApplication::translate("AddUser", "Name:", 0));
        label->setText(QApplication::translate("AddUser", "Enter a name for the new user and select their display color", 0));
        submitButton->setText(QApplication::translate("AddUser", "Create the user", 0));
        label_3->setText(QApplication::translate("AddUser", "Color:", 0));
        errorLabel->setText(QApplication::translate("AddUser", "How can you talk to someone with no name?", 0));
        colorButton->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class AddUser: public Ui_AddUser {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDUSER_H
