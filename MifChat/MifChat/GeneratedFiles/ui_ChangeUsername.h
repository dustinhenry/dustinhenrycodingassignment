/********************************************************************************
** Form generated from reading UI file 'ChangeUsername.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHANGEUSERNAME_H
#define UI_CHANGEUSERNAME_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ChangeUsername
{
public:
    QHBoxLayout *horizontalLayout;
    QFrame *frame;
    QGridLayout *gridLayout;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *usernameEdit;
    QLabel *label_3;
    QPushButton *colorButton;
    QSpacerItem *horizontalSpacer;
    QPushButton *submitButton;
    QLabel *changeLabel;

    void setupUi(QWidget *ChangeUsername)
    {
        if (ChangeUsername->objectName().isEmpty())
            ChangeUsername->setObjectName(QStringLiteral("ChangeUsername"));
        ChangeUsername->resize(310, 150);
        ChangeUsername->setMinimumSize(QSize(310, 150));
        ChangeUsername->setMaximumSize(QSize(310, 150));
        ChangeUsername->setStyleSheet(QStringLiteral("QFrame{background-color:rgb(61,83,105);}"));
        horizontalLayout = new QHBoxLayout(ChangeUsername);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        frame = new QFrame(ChangeUsername);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout = new QGridLayout(frame);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label = new QLabel(frame);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setFamily(QStringLiteral("Century Gothic"));
        font.setPointSize(11);
        label->setFont(font);
        label->setStyleSheet(QStringLiteral("color:white"));
        label->setAlignment(Qt::AlignCenter);
        label->setWordWrap(true);

        gridLayout->addWidget(label, 0, 0, 1, 4);

        label_2 = new QLabel(frame);
        label_2->setObjectName(QStringLiteral("label_2"));
        QFont font1;
        font1.setFamily(QStringLiteral("Century Gothic"));
        font1.setPointSize(10);
        label_2->setFont(font1);
        label_2->setStyleSheet(QStringLiteral("color:white"));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        usernameEdit = new QLineEdit(frame);
        usernameEdit->setObjectName(QStringLiteral("usernameEdit"));
        usernameEdit->setFont(font);
        usernameEdit->setMaxLength(12);

        gridLayout->addWidget(usernameEdit, 1, 1, 1, 3);

        label_3 = new QLabel(frame);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font1);
        label_3->setStyleSheet(QStringLiteral("color:white"));
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        colorButton = new QPushButton(frame);
        colorButton->setObjectName(QStringLiteral("colorButton"));
        colorButton->setMinimumSize(QSize(30, 25));
        colorButton->setMaximumSize(QSize(30, 25));
        colorButton->setStyleSheet(QStringLiteral("background-color:white"));
        colorButton->setFlat(false);

        gridLayout->addWidget(colorButton, 2, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(58, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 2, 2, 1, 1);

        submitButton = new QPushButton(frame);
        submitButton->setObjectName(QStringLiteral("submitButton"));
        submitButton->setFont(font);

        gridLayout->addWidget(submitButton, 2, 3, 1, 1);

        changeLabel = new QLabel(frame);
        changeLabel->setObjectName(QStringLiteral("changeLabel"));
        QFont font2;
        font2.setFamily(QStringLiteral("Century Gothic"));
        changeLabel->setFont(font2);
        changeLabel->setStyleSheet(QStringLiteral("color:white"));
        changeLabel->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(changeLabel, 3, 0, 1, 4);


        horizontalLayout->addWidget(frame);


        retranslateUi(ChangeUsername);

        QMetaObject::connectSlotsByName(ChangeUsername);
    } // setupUi

    void retranslateUi(QWidget *ChangeUsername)
    {
        ChangeUsername->setWindowTitle(QApplication::translate("ChangeUsername", "ChangeUsername", 0));
        label->setText(QApplication::translate("ChangeUsername", "You never could make up your mind, could you?", 0));
        label_2->setText(QApplication::translate("ChangeUsername", "New Name:", 0));
        label_3->setText(QApplication::translate("ChangeUsername", "New Color:", 0));
        colorButton->setText(QString());
        submitButton->setText(QApplication::translate("ChangeUsername", "Change Me*", 0));
        changeLabel->setText(QApplication::translate("ChangeUsername", "*Real change comes from within.", 0));
    } // retranslateUi

};

namespace Ui {
    class ChangeUsername: public Ui_ChangeUsername {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHANGEUSERNAME_H
