/****************************************************************************
** Meta object code from reading C++ file 'UserController.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../UserController.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'UserController.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_UserController_t {
    QByteArrayData data[12];
    char stringdata[131];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_UserController_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_UserController_t qt_meta_stringdata_UserController = {
    {
QT_MOC_LITERAL(0, 0, 14), // "UserController"
QT_MOC_LITERAL(1, 15, 18), // "primaryUserChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 14), // "ShowUserDialog"
QT_MOC_LITERAL(4, 50, 13), // "AddUserToList"
QT_MOC_LITERAL(5, 64, 4), // "name"
QT_MOC_LITERAL(6, 69, 5), // "color"
QT_MOC_LITERAL(7, 75, 10), // "RemoveUser"
QT_MOC_LITERAL(8, 86, 17), // "SwitchCurrentUser"
QT_MOC_LITERAL(9, 104, 5), // "index"
QT_MOC_LITERAL(10, 110, 11), // "MentionUser"
QT_MOC_LITERAL(11, 122, 8) // "username"

    },
    "UserController\0primaryUserChanged\0\0"
    "ShowUserDialog\0AddUserToList\0name\0"
    "color\0RemoveUser\0SwitchCurrentUser\0"
    "index\0MentionUser\0username"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_UserController[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,   45,    2, 0x0a /* Public */,
       4,    2,   46,    2, 0x0a /* Public */,
       7,    0,   51,    2, 0x0a /* Public */,
       8,    1,   52,    2, 0x0a /* Public */,
      10,    1,   55,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QColor,    5,    6,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    9,
    QMetaType::Void, QMetaType::QString,   11,

       0        // eod
};

void UserController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        UserController *_t = static_cast<UserController *>(_o);
        switch (_id) {
        case 0: _t->primaryUserChanged(); break;
        case 1: _t->ShowUserDialog(); break;
        case 2: _t->AddUserToList((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QColor(*)>(_a[2]))); break;
        case 3: _t->RemoveUser(); break;
        case 4: _t->SwitchCurrentUser((*reinterpret_cast< QModelIndex(*)>(_a[1]))); break;
        case 5: _t->MentionUser((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (UserController::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&UserController::primaryUserChanged)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject UserController::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_UserController.data,
      qt_meta_data_UserController,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *UserController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *UserController::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_UserController.stringdata))
        return static_cast<void*>(const_cast< UserController*>(this));
    return QWidget::qt_metacast(_clname);
}

int UserController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void UserController::primaryUserChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
