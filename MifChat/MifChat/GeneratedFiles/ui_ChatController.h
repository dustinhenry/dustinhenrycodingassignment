/********************************************************************************
** Form generated from reading UI file 'ChatController.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHATCONTROLLER_H
#define UI_CHATCONTROLLER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ChatController
{
public:
    QGridLayout *gridLayout_3;
    QFrame *chatFrame;
    QGridLayout *gridLayout_2;
    QLabel *currentUserLabel;
    QLabel *left;
    QLabel *shadow;
    QLineEdit *chatEdit;
    QLabel *title;
    QLabel *middle;
    QLabel *right;
    QPushButton *chatButton;
    QLabel *label_2;
    QPushButton *emoticonButton;
    QListWidget *chatList;

    void setupUi(QWidget *ChatController)
    {
        if (ChatController->objectName().isEmpty())
            ChatController->setObjectName(QStringLiteral("ChatController"));
        ChatController->resize(1156, 821);
        gridLayout_3 = new QGridLayout(ChatController);
        gridLayout_3->setSpacing(0);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        chatFrame = new QFrame(ChatController);
        chatFrame->setObjectName(QStringLiteral("chatFrame"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(chatFrame->sizePolicy().hasHeightForWidth());
        chatFrame->setSizePolicy(sizePolicy);
        chatFrame->setMinimumSize(QSize(400, 0));
        QFont font;
        font.setFamily(QStringLiteral("Century Gothic"));
        font.setPointSize(10);
        chatFrame->setFont(font);
        chatFrame->setStyleSheet(QStringLiteral("QFrame{background-color:rgb(61,83,105);}"));
        chatFrame->setFrameShape(QFrame::Box);
        chatFrame->setFrameShadow(QFrame::Plain);
        chatFrame->setLineWidth(0);
        gridLayout_2 = new QGridLayout(chatFrame);
        gridLayout_2->setSpacing(0);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 4);
        currentUserLabel = new QLabel(chatFrame);
        currentUserLabel->setObjectName(QStringLiteral("currentUserLabel"));
        QFont font1;
        font1.setFamily(QStringLiteral("Century Gothic"));
        font1.setPointSize(11);
        currentUserLabel->setFont(font1);
        currentUserLabel->setIndent(4);

        gridLayout_2->addWidget(currentUserLabel, 0, 3, 1, 2);

        left = new QLabel(chatFrame);
        left->setObjectName(QStringLiteral("left"));
        left->setMaximumSize(QSize(4, 16777215));
        left->setFont(font);
        left->setPixmap(QPixmap(QString::fromUtf8("Resources/vertical_seperator.png")));
        left->setScaledContents(true);

        gridLayout_2->addWidget(left, 0, 0, 5, 1);

        shadow = new QLabel(chatFrame);
        shadow->setObjectName(QStringLiteral("shadow"));
        shadow->setStyleSheet(QStringLiteral("background-color:rgb(212,217,221);"));
        shadow->setPixmap(QPixmap(QString::fromUtf8("Resources/shadow.png")));
        shadow->setScaledContents(true);

        gridLayout_2->addWidget(shadow, 1, 1, 1, 4);

        chatEdit = new QLineEdit(chatFrame);
        chatEdit->setObjectName(QStringLiteral("chatEdit"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(chatEdit->sizePolicy().hasHeightForWidth());
        chatEdit->setSizePolicy(sizePolicy1);
        chatEdit->setMinimumSize(QSize(0, 30));
        chatEdit->setMaximumSize(QSize(16777215, 30));
        chatEdit->setFont(font);
        chatEdit->setStyleSheet(QStringLiteral(""));
        chatEdit->setMaxLength(140);

        gridLayout_2->addWidget(chatEdit, 4, 1, 1, 2);

        title = new QLabel(chatFrame);
        title->setObjectName(QStringLiteral("title"));
        QSizePolicy sizePolicy2(QSizePolicy::Ignored, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(title->sizePolicy().hasHeightForWidth());
        title->setSizePolicy(sizePolicy2);
        title->setMinimumSize(QSize(0, 30));
        QFont font2;
        font2.setFamily(QStringLiteral("Century Gothic"));
        font2.setPointSize(11);
        font2.setBold(false);
        font2.setWeight(50);
        title->setFont(font2);
        title->setStyleSheet(QStringLiteral("Color:white;"));

        gridLayout_2->addWidget(title, 0, 1, 1, 1);

        middle = new QLabel(chatFrame);
        middle->setObjectName(QStringLiteral("middle"));
        middle->setMaximumSize(QSize(16777215, 4));

        gridLayout_2->addWidget(middle, 3, 1, 1, 4);

        right = new QLabel(chatFrame);
        right->setObjectName(QStringLiteral("right"));
        right->setMaximumSize(QSize(4, 16777215));
        right->setFont(font);
        right->setLineWidth(0);
        right->setPixmap(QPixmap(QString::fromUtf8("Resources/vertical_seperator.png")));
        right->setScaledContents(true);

        gridLayout_2->addWidget(right, 0, 5, 5, 1);

        chatButton = new QPushButton(chatFrame);
        chatButton->setObjectName(QStringLiteral("chatButton"));
        QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(chatButton->sizePolicy().hasHeightForWidth());
        chatButton->setSizePolicy(sizePolicy3);
        chatButton->setMinimumSize(QSize(0, 30));
        chatButton->setMaximumSize(QSize(120, 30));
        chatButton->setFont(font);

        gridLayout_2->addWidget(chatButton, 4, 4, 1, 1);

        label_2 = new QLabel(chatFrame);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMaximumSize(QSize(100, 16777215));
        label_2->setFont(font1);
        label_2->setStyleSheet(QStringLiteral("color:white"));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_2->setMargin(0);
        label_2->setIndent(8);

        gridLayout_2->addWidget(label_2, 0, 2, 1, 1);

        emoticonButton = new QPushButton(chatFrame);
        emoticonButton->setObjectName(QStringLiteral("emoticonButton"));
        sizePolicy3.setHeightForWidth(emoticonButton->sizePolicy().hasHeightForWidth());
        emoticonButton->setSizePolicy(sizePolicy3);
        emoticonButton->setMinimumSize(QSize(35, 30));
        emoticonButton->setMaximumSize(QSize(35, 30));
        QFont font3;
        font3.setFamily(QStringLiteral("Wingdings"));
        font3.setPointSize(16);
        font3.setBold(false);
        font3.setItalic(false);
        font3.setWeight(50);
        emoticonButton->setFont(font3);

        gridLayout_2->addWidget(emoticonButton, 4, 3, 1, 1);

        chatList = new QListWidget(chatFrame);
        chatList->setObjectName(QStringLiteral("chatList"));
        chatList->setFont(font);
        chatList->setToolTipDuration(-1);
        chatList->setStyleSheet(QStringLiteral("background-color:rgb(212,217,221);"));
        chatList->setFrameShape(QFrame::Box);
        chatList->setFrameShadow(QFrame::Plain);
        chatList->setLineWidth(0);

        gridLayout_2->addWidget(chatList, 2, 1, 1, 4);


        gridLayout_3->addWidget(chatFrame, 0, 0, 1, 1);


        retranslateUi(ChatController);

        QMetaObject::connectSlotsByName(ChatController);
    } // setupUi

    void retranslateUi(QWidget *ChatController)
    {
        ChatController->setWindowTitle(QApplication::translate("ChatController", "ChatController", 0));
        currentUserLabel->setText(QString());
        left->setText(QString());
        shadow->setText(QString());
        chatEdit->setText(QString());
        title->setText(QApplication::translate("ChatController", "   Have a chat with all of the voices in your head", 0));
        middle->setText(QString());
        right->setText(QString());
        chatButton->setText(QApplication::translate("ChatController", "Chat", 0));
        label_2->setText(QApplication::translate("ChatController", "Chatting As:", 0));
        emoticonButton->setText(QApplication::translate("ChatController", "J", 0));
    } // retranslateUi

};

namespace Ui {
    class ChatController: public Ui_ChatController {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHATCONTROLLER_H
