#ifndef CHANGEUSERNAME_H
#define CHANGEUSERNAME_H

#include <QWidget>
#include "ui_ChangeUsername.h"

class ChangeUsername : public QWidget
{
	Q_OBJECT

public:
	ChangeUsername(QWidget *parent = 0);
	~ChangeUsername();

public slots:
	void Show();

signals:
	void userChanged(QString name, QColor color);

	public slots:
	void LaunchColorPicker();
	void ChangeUser();

private:
	Ui::ChangeUsername ui;

	QColor		m_userColor;
};

#endif // CHANGEUSERNAME_H
