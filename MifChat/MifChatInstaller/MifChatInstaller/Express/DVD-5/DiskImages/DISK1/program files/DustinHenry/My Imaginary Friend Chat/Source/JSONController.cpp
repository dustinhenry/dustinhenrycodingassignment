#include "JSONController.h"

JSONController* JSONController::pInstance = 0;

JSONController::JSONController()
	: QWidget(nullptr)
{
	ui.setupUi(this);
}

JSONController::~JSONController()
{

}

void JSONController::ShowJSON(QString json)
{
	ui.JSONText->setText(json);
}
