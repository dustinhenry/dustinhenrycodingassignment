﻿#ifndef WELCOMEPAGE_H
#define WELCOMEPAGE_H

#include <QWidget>
#include "ui_WelcomePage.h"

class WelcomePage : public QWidget
{
	Q_OBJECT

public:
	WelcomePage(QWidget *parent = 0);
	~WelcomePage();

	void Show();

public slots:
	void SubmitPage();
	void FadeInWelcome(int);
	void FadeInUser(int);
	void WelcomeFinished();

signals:
	void Submitted(QString name, QColor color);

private:
	Ui::WelcomePage ui;

	bool	m_bWelcomBack;
	QString	m_stWelcomBackUser;
	QColor	m_userColor;
};

#endif // WELCOMEPAGE_H
