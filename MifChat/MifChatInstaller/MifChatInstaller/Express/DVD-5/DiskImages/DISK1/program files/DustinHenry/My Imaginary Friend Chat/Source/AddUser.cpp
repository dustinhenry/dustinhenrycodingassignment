#include "AddUser.h"
#include <QColorDialog>

AddUser::AddUser(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	QObject::connect(ui.colorButton, SIGNAL(pressed()), this, SLOT(LaunchColorPicker()));
	QObject::connect(ui.submitButton, SIGNAL(pressed()), this, SLOT(SubmitUser()));
	QObject::connect(ui.usernameEdit, SIGNAL(selectionChanged()), ui.errorLabel, SLOT(hide()));

	ui.errorLabel->hide();

	m_userColor = QColor(255, 255, 255);
}

AddUser::~AddUser()
{

}

void AddUser::LaunchColorPicker()
{
	m_userColor = QColorDialog::getColor();
	ui.colorButton->setStyleSheet(QString("background:rgb(%1,%2,%3);").arg(m_userColor.red()).arg(m_userColor.green()).arg(m_userColor.blue()));
}

void AddUser::SubmitUser()
{
	if (ui.usernameEdit->text().isEmpty())
	{
		ui.errorLabel->show();
	}
	else
	{
		QString name = ui.usernameEdit->text();
		if (name.startsWith('@'))
		{
			name.remove('@');
		}
		emit userAdded(name, m_userColor);
		this->close();
	}
}
