/********************************************************************************
** Form generated from reading UI file 'MifChat.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MIFCHAT_H
#define UI_MIFCHAT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MifChatClass
{
public:
    QAction *actionLoad_Conversation;
    QAction *actionSave_Conversation;
    QAction *actionExit;
    QAction *actionChange_Username;
    QAction *actionClear_QSettings;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QHBoxLayout *mainLayout;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuAbout;
    QMenu *menuEdit;

    void setupUi(QMainWindow *MifChatClass)
    {
        if (MifChatClass->objectName().isEmpty())
            MifChatClass->setObjectName(QStringLiteral("MifChatClass"));
        MifChatClass->resize(1280, 687);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MifChatClass->sizePolicy().hasHeightForWidth());
        MifChatClass->setSizePolicy(sizePolicy);
        MifChatClass->setMinimumSize(QSize(805, 420));
        QFont font;
        font.setFamily(QStringLiteral("Century Gothic"));
        font.setPointSize(10);
        font.setKerning(true);
        font.setStyleStrategy(QFont::PreferAntialias);
        MifChatClass->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral("Resources/taskbar_icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        MifChatClass->setWindowIcon(icon);
        MifChatClass->setWindowOpacity(1);
        MifChatClass->setAutoFillBackground(true);
        MifChatClass->setStyleSheet(QStringLiteral("QWidget#centralWidget{background-color:rgb(61,83,105);}"));
        actionLoad_Conversation = new QAction(MifChatClass);
        actionLoad_Conversation->setObjectName(QStringLiteral("actionLoad_Conversation"));
        actionSave_Conversation = new QAction(MifChatClass);
        actionSave_Conversation->setObjectName(QStringLiteral("actionSave_Conversation"));
        actionExit = new QAction(MifChatClass);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        actionChange_Username = new QAction(MifChatClass);
        actionChange_Username->setObjectName(QStringLiteral("actionChange_Username"));
        actionClear_QSettings = new QAction(MifChatClass);
        actionClear_QSettings->setObjectName(QStringLiteral("actionClear_QSettings"));
        centralWidget = new QWidget(MifChatClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setMinimumSize(QSize(620, 0));
        centralWidget->setStyleSheet(QStringLiteral(""));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        mainLayout = new QHBoxLayout();
        mainLayout->setSpacing(0);
        mainLayout->setObjectName(QStringLiteral("mainLayout"));

        gridLayout->addLayout(mainLayout, 0, 0, 1, 1);

        MifChatClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MifChatClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1280, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuAbout = new QMenu(menuBar);
        menuAbout->setObjectName(QStringLiteral("menuAbout"));
        menuEdit = new QMenu(menuBar);
        menuEdit->setObjectName(QStringLiteral("menuEdit"));
        MifChatClass->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEdit->menuAction());
        menuBar->addAction(menuAbout->menuAction());
        menuFile->addAction(actionClear_QSettings);
        menuFile->addAction(actionExit);
        menuEdit->addAction(actionChange_Username);

        retranslateUi(MifChatClass);

        QMetaObject::connectSlotsByName(MifChatClass);
    } // setupUi

    void retranslateUi(QMainWindow *MifChatClass)
    {
        MifChatClass->setWindowTitle(QApplication::translate("MifChatClass", "My Imaginary Friends Chat", 0));
        actionLoad_Conversation->setText(QApplication::translate("MifChatClass", "Load Conversation", 0));
        actionSave_Conversation->setText(QApplication::translate("MifChatClass", "Save Conversation", 0));
        actionExit->setText(QApplication::translate("MifChatClass", "Exit", 0));
        actionChange_Username->setText(QApplication::translate("MifChatClass", "Change Current User", 0));
        actionClear_QSettings->setText(QApplication::translate("MifChatClass", "Clear QSettings", 0));
        menuFile->setTitle(QApplication::translate("MifChatClass", "File", 0));
        menuAbout->setTitle(QApplication::translate("MifChatClass", "About", 0));
        menuEdit->setTitle(QApplication::translate("MifChatClass", "Edit", 0));
    } // retranslateUi

};

namespace Ui {
    class MifChatClass: public Ui_MifChatClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MIFCHAT_H
