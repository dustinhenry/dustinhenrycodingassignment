#include "MifChat.h"
#include "EmoticonsParser.h"
#include "UserController.h"
#include "JSONController.h"
#include "ChatController.h"
#include "WelcomePage.h"
#include "ChangeUsername.h"
#include <QHBoxLayout>
#include <QSettings>

MifChat::MifChat(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	// Initialize all of our singletom classes
	m_pUserController	= UserController::GetInstance();
	m_pChatController	= ChatController::GetInstance();
	m_pJSONController	= JSONController::GetInstance();

	// create the name changer
	m_pNameChanger = new ChangeUsername();
	QObject::connect(m_pNameChanger, SIGNAL(userChanged(QString, QColor)), this, SLOT(ChangePrimaryUser(QString, QColor)));

	// create the welcome page and show it
	m_pWelcomePage = new WelcomePage();
	m_pWelcomePage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	ui.mainLayout->addWidget(m_pWelcomePage);
	m_pWelcomePage->Show();

	// Connect the signin button
	QObject::connect(m_pWelcomePage, SIGNAL(Submitted(QString, QColor)), this, SLOT(WelcomPageSubmitted(QString, QColor)));

	// connect the menu actions
	QObject::connect(ui.actionChange_Username, SIGNAL(triggered(bool)), m_pNameChanger, SLOT(Show()));
	QObject::connect(ui.actionExit, SIGNAL(triggered(bool)), QApplication::instance(), SLOT(quit()));
	QObject::connect(ui.actionClear_QSettings, SIGNAL(triggered(bool)), this, SLOT(ClearQSettings()));
}

MifChat::~MifChat()
{
}
void MifChat::ClearQSettings()
{

	QSettings settings("HardSoft", "MifChat");
	settings.clear();
}

void MifChat::WelcomPageSubmitted(QString username, QColor userColor)
{
	m_pWelcomePage->hide();
	ui.mainLayout->removeWidget(m_pWelcomePage);
	// Add all of our components to the main layout
	ui.mainLayout->addWidget(m_pUserController);
	ui.mainLayout->addWidget(m_pChatController);
	ui.mainLayout->addWidget(m_pJSONController);

	m_pUserController->SetPrimaryUser(username, userColor);
}

void MifChat::ChangePrimaryUser(QString user, QColor color)
{
	m_pUserController->SetPrimaryUser(user, color);
}
