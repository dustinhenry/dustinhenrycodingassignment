/********************************************************************************
** Form generated from reading UI file 'EmoticonPage.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EMOTICONPAGE_H
#define UI_EMOTICONPAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_EmoticonPage
{
public:
    QGridLayout *gridLayout;
    QTableWidget *tableWidget;

    void setupUi(QWidget *EmoticonPage)
    {
        if (EmoticonPage->objectName().isEmpty())
            EmoticonPage->setObjectName(QStringLiteral("EmoticonPage"));
        EmoticonPage->resize(200, 400);
        EmoticonPage->setMinimumSize(QSize(200, 400));
        EmoticonPage->setMaximumSize(QSize(300, 400));
        gridLayout = new QGridLayout(EmoticonPage);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        tableWidget = new QTableWidget(EmoticonPage);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        QFont font;
        font.setFamily(QStringLiteral("Century Gothic"));
        font.setPointSize(11);
        tableWidget->setFont(font);
        tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableWidget->setShowGrid(false);
        tableWidget->setWordWrap(false);
        tableWidget->setCornerButtonEnabled(false);
        tableWidget->horizontalHeader()->setVisible(false);
        tableWidget->horizontalHeader()->setHighlightSections(false);
        tableWidget->verticalHeader()->setVisible(false);
        tableWidget->verticalHeader()->setHighlightSections(false);

        gridLayout->addWidget(tableWidget, 0, 0, 1, 1);


        retranslateUi(EmoticonPage);

        QMetaObject::connectSlotsByName(EmoticonPage);
    } // setupUi

    void retranslateUi(QWidget *EmoticonPage)
    {
        EmoticonPage->setWindowTitle(QApplication::translate("EmoticonPage", "EmoticonPage", 0));
    } // retranslateUi

};

namespace Ui {
    class EmoticonPage: public Ui_EmoticonPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EMOTICONPAGE_H
