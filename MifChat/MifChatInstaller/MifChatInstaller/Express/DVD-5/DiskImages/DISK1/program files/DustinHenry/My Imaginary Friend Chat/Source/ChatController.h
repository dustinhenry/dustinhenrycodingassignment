#ifndef CHATCONTROLLER_H
#define CHATCONTROLLER_H

#include <QWidget>
#include <QComboBox>
#include "ui_ChatController.h"

class EmoticonPage;
class EmoticonsParser;
class EmoticonsImg;
class UserController;
class QNetworkReply;
class QNetworkAccessManager;

enum ChatStringElementType
{
	Emoticon,
	Mention,
	URL
};

struct ChatStringElement
{
	ChatStringElement(QString resource, int start, int end, ChatStringElementType type) :
		m_resource(resource), m_start(start), m_end(end), m_type(type) {}

	bool operator< (const ChatStringElement& other)
	{
		return m_start < other.m_start;
	}

	int GetLength(int stringSize)
	{
		return (m_end == -1) ? stringSize - m_start : m_end - m_start;
	}
	void Stretch(int stretchLength)
	{
		m_start += stretchLength;
		if (m_end != -1)
		{
			m_end += stretchLength;
		}
	}

	QString					m_resource;
	int						m_start;
	int						m_end;
	ChatStringElementType	m_type;
};

struct JSONDocumentContents
{
	std::vector<ChatStringElement> vEmoticons;
	std::vector<ChatStringElement> vMentions;
	std::vector<ChatStringElement> vURLs;
	std::map<QString, QString> vURLTitles;

	void Clear()
	{
		vEmoticons.clear();
		vMentions.clear();
		vURLs.clear();
		vURLTitles.clear();
	}
};

class ChatController : public QWidget
{
	Q_OBJECT

public:
	~ChatController(void);

	static void DeleteInstance(void)
	{
		if (pInstance)
		{
			delete pInstance;
			pInstance = 0;
		}
	}
	static ChatController *GetInstance()
	{
		// Check to see if it has been made
		if (pInstance == 0)
			pInstance = new ChatController();
		// Return the address of the instance
		return pInstance;
	}

	void AddEmoticon(QString name);
	void CreateJSONDocument();
	void QueryURLTitles();
	void AddMesageToChatList();

public slots:
	void ChangePrimaryUser();
	void ShowEmoticons();
	void ChatPressed();
	void EditLostFocus();
	void EditCursorPositionChanged(int old, int current);
	void NetworkManagerFinished(QNetworkReply *reply);

private:
	// Instance of the class
	static ChatController *pInstance;
	// Make it a singleton by declaring constructors private
	ChatController();
	ChatController(const ChatController&);
	ChatController operator = (const ChatController&);

	Ui::ChatController ui;

	QNetworkAccessManager*	m_pNetworkManager;
	EmoticonPage*			m_pEmoticonPage;
	UserController*			m_pUserController;
	EmoticonsParser*		m_pEmoticonsParser;
	std::map<std::string, EmoticonsImg> const* m_pEmoticonMap;

	QString					m_currentChatString;
	std::vector<QString>	m_vChatLog;
	std::vector<QString>	m_vChatLogHTML;

	int						m_editPosition;

	JSONDocumentContents	m_jsonDocumentContents;
};

#endif // CHATCONTROLLER_H
