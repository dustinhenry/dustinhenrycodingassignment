#ifndef ADDUSER_H
#define ADDUSER_H

#include <QWidget>
#include "ui_AddUser.h"

class AddUser : public QWidget
{
	Q_OBJECT

public:
	AddUser(QWidget *parent = 0);
	~AddUser();

signals:
	void userAdded(QString name, QColor color);

public slots:
	void LaunchColorPicker();
	void SubmitUser();

private:
	Ui::AddUser ui;

	QColor		m_userColor;
};

#endif // ADDUSER_H
