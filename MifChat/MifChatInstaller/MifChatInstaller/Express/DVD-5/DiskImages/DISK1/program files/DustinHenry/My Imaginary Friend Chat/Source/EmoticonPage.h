#ifndef EMOTICONPAGE_H
#define EMOTICONPAGE_H

#include <QWidget>
#include "ui_EmoticonPage.h"

class EmoticonsParser;
class EmoticonsImg;
class UserController;
class QNetworkReply;
class QNetworkAccessManager;

class EmoticonPage : public QWidget
{
	Q_OBJECT

public:
	~EmoticonPage(void);

	static void DeleteInstance(void)
	{
		if (pInstance)
		{
			delete pInstance;
			pInstance = 0;
		}
	}
	static EmoticonPage *GetInstance(QWidget *parent)
	{
		// Check to see if it has been made
		if (pInstance == 0)
			pInstance = new EmoticonPage(parent);
		// Return the address of the instance
		return pInstance;
	}
	QPixmap GetImagePixmap(QString name);

public slots:
	void NetworkManagerFinished(QNetworkReply *reply);
	void Show();
	void EmoticonSelected(QModelIndex);

private:
	// Instance of the class
	static EmoticonPage *pInstance;
	// Make it a singleton by declaring constructors private
	EmoticonPage(QWidget *parent);
	EmoticonPage(const EmoticonPage&);
	EmoticonPage operator = (const EmoticonPage&);

	Ui::EmoticonPage ui;

	QNetworkAccessManager*	m_pNetworkManager;
	EmoticonsParser*		m_pEmoticonsParser;

	std::map<std::string, EmoticonsImg> const*	m_pEmoticonMap;
	std::map<std::string, QPixmap>				m_imageMap;
};

#endif // EMOTICONPAGE_H
