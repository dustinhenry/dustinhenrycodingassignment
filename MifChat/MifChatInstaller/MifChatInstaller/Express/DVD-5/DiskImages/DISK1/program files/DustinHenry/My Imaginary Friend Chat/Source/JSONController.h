#ifndef JSONCONTROLLER_H
#define JSONCONTROLLER_H

#include <QWidget>
#include "ui_JSONController.h"

class JSONController : public QWidget
{
	Q_OBJECT

public:
	~JSONController(void);

	static void DeleteInstance(void)
	{
		if (pInstance)
		{
			delete pInstance;
			pInstance = 0;
		}
	}
	static JSONController *GetInstance()
	{
		// Check to see if it has been made
		if (pInstance == 0)
			pInstance = new JSONController();
		// Return the address of the instance
		return pInstance;
	}

public slots:
	void ShowJSON(QString json);

private:
	// Instance of the class
	static JSONController *pInstance;
	// Make it a singleton by declaring constructors private
	JSONController();
	JSONController(const JSONController&);
	JSONController operator = (const JSONController&);

	Ui::JSONController ui;
};

#endif // JSONCONTROLLER_H
