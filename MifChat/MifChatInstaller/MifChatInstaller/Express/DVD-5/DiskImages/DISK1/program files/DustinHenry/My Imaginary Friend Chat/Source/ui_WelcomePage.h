/********************************************************************************
** Form generated from reading UI file 'WelcomePage.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WELCOMEPAGE_H
#define UI_WELCOMEPAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WelcomePage
{
public:
    QGridLayout *gridLayout_2;
    QFrame *WelcomeBack;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;
    QLabel *welcomLabel;
    QSpacerItem *verticalSpacer_3;
    QGroupBox *usernameBox;
    QGridLayout *gridLayout;
    QPushButton *submitButton;
    QSpacerItem *horizontalSpacer_11;
    QSpacerItem *horizontalSpacer_9;
    QSpacerItem *horizontalSpacer_10;
    QLabel *usernameLabel;
    QSpacerItem *horizontalSpacer_7;
    QSpacerItem *horizontalSpacer_8;
    QSpacerItem *horizontalSpacer_12;
    QLineEdit *usernameEdit;
    QLabel *errorLabel;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QWidget *WelcomePage)
    {
        if (WelcomePage->objectName().isEmpty())
            WelcomePage->setObjectName(QStringLiteral("WelcomePage"));
        WelcomePage->resize(765, 539);
        gridLayout_2 = new QGridLayout(WelcomePage);
        gridLayout_2->setSpacing(0);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        WelcomeBack = new QFrame(WelcomePage);
        WelcomeBack->setObjectName(QStringLiteral("WelcomeBack"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(WelcomeBack->sizePolicy().hasHeightForWidth());
        WelcomeBack->setSizePolicy(sizePolicy);
        WelcomeBack->setStyleSheet(QStringLiteral("QFrame#WelcomeBack{background-color:rgb(61,83,105);}"));
        WelcomeBack->setFrameShape(QFrame::StyledPanel);
        WelcomeBack->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(WelcomeBack);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalSpacer = new QSpacerItem(20, 187, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        welcomLabel = new QLabel(WelcomeBack);
        welcomLabel->setObjectName(QStringLiteral("welcomLabel"));
        QFont font;
        font.setFamily(QStringLiteral("Century Gothic"));
        font.setPointSize(16);
        welcomLabel->setFont(font);
        welcomLabel->setStyleSheet(QStringLiteral("color:white"));
        welcomLabel->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(welcomLabel);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_3);

        usernameBox = new QGroupBox(WelcomeBack);
        usernameBox->setObjectName(QStringLiteral("usernameBox"));
        QFont font1;
        font1.setFamily(QStringLiteral("Century Gothic"));
        font1.setPointSize(11);
        usernameBox->setFont(font1);
        usernameBox->setAlignment(Qt::AlignCenter);
        usernameBox->setFlat(true);
        usernameBox->setCheckable(false);
        gridLayout = new QGridLayout(usernameBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        submitButton = new QPushButton(usernameBox);
        submitButton->setObjectName(QStringLiteral("submitButton"));
        submitButton->setMaximumSize(QSize(16777215, 16777215));
        submitButton->setFont(font1);

        gridLayout->addWidget(submitButton, 3, 2, 1, 1);

        horizontalSpacer_11 = new QSpacerItem(123, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_11, 2, 4, 1, 1);

        horizontalSpacer_9 = new QSpacerItem(124, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_9, 0, 0, 1, 1);

        horizontalSpacer_10 = new QSpacerItem(124, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_10, 1, 0, 2, 1);

        usernameLabel = new QLabel(usernameBox);
        usernameLabel->setObjectName(QStringLiteral("usernameLabel"));
        QFont font2;
        font2.setFamily(QStringLiteral("Century Gothic"));
        font2.setPointSize(12);
        usernameLabel->setFont(font2);
        usernameLabel->setStyleSheet(QStringLiteral("color:white"));
        usernameLabel->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(usernameLabel, 0, 1, 1, 3);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_7, 0, 4, 1, 1);

        horizontalSpacer_8 = new QSpacerItem(123, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_8, 2, 1, 1, 1);

        horizontalSpacer_12 = new QSpacerItem(124, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_12, 1, 3, 2, 1);

        usernameEdit = new QLineEdit(usernameBox);
        usernameEdit->setObjectName(QStringLiteral("usernameEdit"));
        usernameEdit->setMaximumSize(QSize(200, 16777215));
        usernameEdit->setFont(font1);
        usernameEdit->setMaxLength(12);
        usernameEdit->setFrame(true);
        usernameEdit->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(usernameEdit, 1, 2, 2, 1);

        errorLabel = new QLabel(usernameBox);
        errorLabel->setObjectName(QStringLiteral("errorLabel"));
        errorLabel->setFont(font1);
        errorLabel->setStyleSheet(QStringLiteral("color:rgb(250,136,147);"));
        errorLabel->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(errorLabel, 4, 1, 1, 3);


        verticalLayout->addWidget(usernameBox);

        verticalSpacer_2 = new QSpacerItem(20, 187, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);


        gridLayout_2->addWidget(WelcomeBack, 0, 0, 1, 1);


        retranslateUi(WelcomePage);

        QMetaObject::connectSlotsByName(WelcomePage);
    } // setupUi

    void retranslateUi(QWidget *WelcomePage)
    {
        WelcomePage->setWindowTitle(QApplication::translate("WelcomePage", "WelcomePage", 0));
        welcomLabel->setText(QApplication::translate("WelcomePage", "Welcome to My Imaginary Friends Chat", 0));
        usernameBox->setTitle(QString());
        submitButton->setText(QApplication::translate("WelcomePage", "Sign me up!", 0));
        usernameLabel->setText(QApplication::translate("WelcomePage", "Why don't you make yourself a username", 0));
        usernameEdit->setText(QString());
        errorLabel->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class WelcomePage: public Ui_WelcomePage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WELCOMEPAGE_H
