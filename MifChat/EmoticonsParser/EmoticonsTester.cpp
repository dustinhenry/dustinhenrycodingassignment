#include <iostream>
#include <tchar.h>
#include <time.h>
#include <windows.h>
#include <conio.h>
#include "EmoticonsParser.h"

int _tmain(int argc, _TCHAR* argv[])
{
	EmoticonsParser* pEmoticons = EmoticonsParser::GetInstance();
	string stFileBuffer;

	char cBuffer[256];

	if (pEmoticons->Init("TestFile.xml"))
	{
		EmoticonsDocument *pDocument = &(pEmoticons->GetData()->m_Document);
		sprintf_s(cBuffer, "ELEMENT: Document\n"); stFileBuffer += cBuffer;
		vector<EmoticonsEmoticon>::iterator pEmoticon = pDocument->vEmoticon.begin();
		for (; pEmoticon != pDocument->vEmoticon.end(); pEmoticon++)
		{
			sprintf_s(cBuffer, "	ELEMENT: Emoticon\n"); stFileBuffer += cBuffer;
			sprintf_s(cBuffer, "		ATTRIBUTE: Name - Value = %s\n", pEmoticon->stName.c_str()); stFileBuffer += cBuffer;
			vector<EmoticonsImg>::iterator pImg = pEmoticon->vImg.begin();
			for (; pImg != pEmoticon->vImg.end(); pImg++)
			{
				sprintf_s(cBuffer, "		ELEMENT: Img\n"); stFileBuffer += cBuffer;
				sprintf_s(cBuffer, "			ATTRIBUTE: SRC - Value = %s\n", pImg->stSRC.c_str()); stFileBuffer += cBuffer;
				sprintf_s(cBuffer, "			ATTRIBUTE: Height - Value = %d\n", pImg->iHeight); stFileBuffer += cBuffer;
				sprintf_s(cBuffer, "			ATTRIBUTE: Width - Value = %d\n", pImg->iWidth); stFileBuffer += cBuffer;
			}
		}
		printf(stFileBuffer.c_str());
		FILE *pFile = 0; fopen_s(&pFile, "TestOutput.txt", "w");
		fwrite(stFileBuffer.c_str(),1,stFileBuffer.length(),pFile);
		fclose(pFile);
	    pEmoticons->Save("UpdatedXML.xml");
	}
	pEmoticons->DeleteInstance();	_getch();	return 0;
}
